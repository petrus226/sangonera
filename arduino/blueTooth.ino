const int LED_GREEN = 13;

char message = 0;                

void setup()  
{
  Serial.begin(9600);
  pinMode(LED_GREEN, OUTPUT);
}

void loop() 
{
  if(Serial.available() > 0)  
    {
      message = Serial.read();      
      Serial.print(message);        
      Serial.print("\n");         
      if(message == '1')           
        digitalWrite(LED_GREEN, HIGH);  
      else if(message == '0')
        digitalWrite(LED_RED, LOW);   
    }      
 
}