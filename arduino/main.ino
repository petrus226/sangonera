const int rele5v =  13;
const int radioPin =  12;

String message = "";                

void setup() {
  Serial.begin(9600);

  pinMode(rele5v, OUTPUT);
  digitalWrite(rele5v, HIGH);
  pinMode(radioPin, OUTPUT);
}

void loop() {
   if(Serial.available() > 0)  
    {
      message = Serial.readString();    
      Serial.print(message);        
      Serial.print("\n");         
      if(message == "aComer_aComer") {          
        pushButton();
      }
    }      
}

void pushButton() {
  digitalWrite(radioPin, HIGH);
  delay(1000);
  digitalWrite(radioPin, LOW);
}